package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;
import java.util.Date;

import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Interface CalculateGBCEAction.
 */
public interface CalculateGBCEAction extends Action {

	/**
	 * Calculates GBCE in a given range.
	 * 
	 * @param startTime
	 *            the start time
	 * @param endTime
	 *            the end time
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculateGBCE(Date startTime, Date endTime)
			throws ExecuteException;
}
