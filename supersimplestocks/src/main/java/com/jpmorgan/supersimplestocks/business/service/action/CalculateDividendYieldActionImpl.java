package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.businessrule.CalculateDividendYieldBR;
import com.jpmorgan.supersimplestocks.businessrule.CalculateDividendYieldBRFactory;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;

/**
 * The Class CalculateDividendYieldActionImpl.
 */
public class CalculateDividendYieldActionImpl implements
		CalculateDividendYieldAction {

	/** The calculate dividend yield br factory. */
	@Autowired
	private CalculateDividendYieldBRFactory calculateDividendYieldBRFactory;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculateDividendYieldActionImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.supersimplestocks.business.service.action.
	 * CalculateDividendYieldAction
	 * #calculateDividendYield(com.jpmorgan.supersimplestocks
	 * .business.model.Stock)
	 */
	@Override
	public BigDecimal calculateDividendYield(Stock stock)
			throws ExecuteException {

		// check if stock is null
		if (stock == null) {
			NullStockException nullStockException = new NullStockException();
			LOGGER.error(nullStockException.getMsg());
			throw nullStockException;
		}

		// retrieve the right CalculateDividendYieldBR from factory
		CalculateDividendYieldBR calculateDIvidendYieldBR = calculateDividendYieldBRFactory
				.calculateDividendYieldBRFactory(stock.getType());

		// return dividend yield
		return calculateDIvidendYieldBR.calculateDividendYield(stock);
	}

}
