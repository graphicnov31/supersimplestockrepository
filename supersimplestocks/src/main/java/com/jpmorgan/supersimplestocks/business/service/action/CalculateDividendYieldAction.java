package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Interface CalculateDividendYieldAction.
 */
public interface CalculateDividendYieldAction extends Action {

	/**
	 * Calculates the dividend yield.
	 * 
	 * @param stock
	 *            the stock
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculateDividendYield(Stock stock) throws ExecuteException;
}
