package com.jpmorgan.supersimplestocks.business.model;

/**
 * The Enum StockType.
 */
public enum StockType {

	COMMON, PREFERRED;

}
