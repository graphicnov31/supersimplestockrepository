package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.service.StockBusinessService;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;

/**
 * The Class CalculatePERatioActionImpl.
 */
public class CalculatePERatioActionImpl implements CalculatePERatioAction {

	/** The stock business service. */
	@Autowired
	private StockBusinessService stockBusinessService;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculateDividendYieldActionImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.action.CalculatePERatioAction
	 * #calculatePERation(com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public BigDecimal calculatePERatio(Stock stock) throws ExecuteException {
		// check if stock is null
		if (stock == null) {
			NullStockException nullStockException = new NullStockException();
			LOGGER.error(nullStockException.getMsg());
			throw nullStockException;
		}

		// calculate dividendYield
		BigDecimal dividend = stockBusinessService
				.calculateDividendYield(stock);
		// return tickerPrice / dividendYield
		return stock.getTickerPrice().divide(dividend, 2,
				BigDecimal.ROUND_HALF_UP);
	}

}
