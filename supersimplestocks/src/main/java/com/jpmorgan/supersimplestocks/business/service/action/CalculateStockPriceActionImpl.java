package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.Trade;
import com.jpmorgan.supersimplestocks.entity.service.StockEntityService;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;

/**
 * The Class CalculateStockPriceActionImpl.
 */
public class CalculateStockPriceActionImpl implements CalculateStockPriceAction {

	/** The stock entity service. */
	@Autowired
	private StockEntityService stockEntityService;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculateDividendYieldActionImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.supersimplestocks.business.service.action.
	 * CalculateStockPriceAction
	 * #calculateStockPrice(com.jpmorgan.supersimplestocks.business.model.Stock,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public BigDecimal calculateStockPrice(Stock stock, Date startTime,
			Date endTime) throws ExecuteException {
		// check if stock is null
		if (stock == null) {
			NullStockException nullStockException = new NullStockException();
			LOGGER.error(nullStockException.getMsg());
			throw nullStockException;
		}

		// retrieve trades in a range for the given stock
		Collection<Trade> trades = stockEntityService.getTradesInRange(stock,
				startTime, endTime);

		BigDecimal priceXquantity = BigDecimal.ZERO;
		BigDecimal totalQuantity = BigDecimal.ZERO;
		if (trades != null) {
			// for each trade
			for (Trade trade : trades) {
				// calculate tradePrice x quantity
				priceXquantity = priceXquantity.add(trade.getPrice().multiply(
						new BigDecimal(trade.getSharesQuantity())));
				// calculate totalQuantity
				totalQuantity = totalQuantity.add(new BigDecimal(trade
						.getSharesQuantity()));
			}
		}
		if (totalQuantity.compareTo(BigDecimal.ZERO) > 0) {
			// return (tradePrice x quantity) / totalQuantity
			return priceXquantity.divide(totalQuantity, 2,
					BigDecimal.ROUND_HALF_UP);
		}
		return BigDecimal.ZERO;
	}

}
