package com.jpmorgan.supersimplestocks.business.service;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.business.service.action.CalculateDividendYieldAction;
import com.jpmorgan.supersimplestocks.business.service.action.CalculateGBCEAction;
import com.jpmorgan.supersimplestocks.business.service.action.CalculatePERatioAction;
import com.jpmorgan.supersimplestocks.business.service.action.CalculateStockPriceAction;
import com.jpmorgan.supersimplestocks.business.service.action.RecordTradeAction;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Class StockBusinessServiceImpl.
 */
public class StockBusinessServiceImpl implements StockBusinessService {

	/** The calculate dividend yield action. */
	@Autowired
	private CalculateDividendYieldAction calculateDividendYieldAction;

	/** The calculate pe ratio action. */
	@Autowired
	private CalculatePERatioAction calculatePERatioAction;

	/** The record trade action. */
	@Autowired
	private RecordTradeAction recordTradeAction;

	/** The calculate stock price action. */
	@Autowired
	private CalculateStockPriceAction calculateStockPriceAction;

	/** The calculate gbce action. */
	@Autowired
	private CalculateGBCEAction calculateGBCEAction;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.StockBusinessService#
	 * calculateDividendYield
	 * (com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public BigDecimal calculateDividendYield(Stock stock)
			throws ExecuteException {
		return calculateDividendYieldAction.calculateDividendYield(stock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.StockBusinessService#
	 * calculatePERatio(com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public BigDecimal calculatePERatio(Stock stock) throws ExecuteException {
		return calculatePERatioAction.calculatePERatio(stock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.StockBusinessService#
	 * recordTrade(com.jpmorgan.supersimplestocks.business.model.Stock,
	 * java.util.Date, int,
	 * com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType,
	 * java.math.BigDecimal)
	 */
	@Override
	public void recordTrade(Stock stock, Date timestamp, int sharesQuantity,
			TradeIndicatorType indicatorType, BigDecimal price)
			throws ExecuteException {
		recordTradeAction.recordTrade(stock, timestamp, sharesQuantity,
				indicatorType, price);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.StockBusinessService#
	 * calculateStockPrice(com.jpmorgan.supersimplestocks.business.model.Stock,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public BigDecimal calculateStockPrice(Stock stock, Date startTime,
			Date endTime) throws ExecuteException {
		return calculateStockPriceAction.calculateStockPrice(stock, startTime,
				endTime);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.StockBusinessService#
	 * calculateGBCE(java.util.Date, java.util.Date)
	 */
	@Override
	public BigDecimal calculateGBCE(Date startTime, Date endTime)
			throws ExecuteException {
		return calculateGBCEAction.calculateGBCE(startTime, endTime);
	}

}
