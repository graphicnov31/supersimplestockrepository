package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.service.StockBusinessService;
import com.jpmorgan.supersimplestocks.entity.service.StockEntityService;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Class CalculateGBCEActionImpl.
 */
public class CalculateGBCEActionImpl implements CalculateGBCEAction {

	/** The stock business service. */
	@Autowired
	private StockBusinessService stockBusinessService;

	/** The stock entity service. */
	@Autowired
	private StockEntityService stockEntityService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.action.CalculateGBCEAction
	 * #calculateGBCE(java.util.Date, java.util.Date)
	 */
	@Override
	public BigDecimal calculateGBCE(Date startTime, Date endTime)
			throws ExecuteException {
		Collection<Stock> stocks = stockEntityService.getStocks();
		BigDecimal finalStockPrice = BigDecimal.ONE;
		// number of stocks which have a positive price
		int notZeroStockPrices = 0;
		if (stocks != null) {
			// for each stock
			for (Stock stock : stocks) {
				// calculate stockPrice
				BigDecimal stockPrice = stockBusinessService
						.calculateStockPrice(stock, startTime, endTime);
				if (stockPrice.compareTo(BigDecimal.ZERO) > 0) {
					// multiply stockPrice to the previous result
					finalStockPrice = finalStockPrice.multiply(stockPrice);
					notZeroStockPrices++;
				}
			}
			if (notZeroStockPrices > 0) {
				// return geometric mean of prices for all stocks
				return BigDecimal.valueOf(
						Math.pow(finalStockPrice.doubleValue(),
								Double.valueOf(1) / (notZeroStockPrices)))
						.setScale(2, BigDecimal.ROUND_HALF_UP);
			} else {
				return BigDecimal.ZERO;
			}
		}

		return BigDecimal.ZERO;
	}
}
