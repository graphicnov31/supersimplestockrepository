package com.jpmorgan.supersimplestocks.business.model;

/**
 * The Enum TradeIndicatorType.
 */
public enum TradeIndicatorType {

	BUY, SELL;
}
