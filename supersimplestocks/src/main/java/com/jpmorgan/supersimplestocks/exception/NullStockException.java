package com.jpmorgan.supersimplestocks.exception;

/**
 * This exception is thrown when the Stock object is null.
 */
public class NullStockException extends ExecuteException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The msg. */
	private String msg = "Stock cannot be null";

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return getMsg();
	}

	/**
	 * Gets the msg.
	 * 
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

}
