package com.jpmorgan.supersimplestocks.businessrule;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;

/**
 * The Class CalculateDividendYieldForPreferredBRImpl.
 */
public class CalculateDividendYieldForPreferredBRImpl implements
		CalculateDividendYieldBR {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculateDividendYieldForPreferredBRImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.businessrule.CalculateDIvidendYieldBR#calculateDividendYield
	 * (com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public BigDecimal calculateDividendYield(Stock stock)
			throws ExecuteException {
		// check if stock is null
		if (stock == null) {
			NullStockException nullStockException = new NullStockException();
			LOGGER.error(nullStockException.getMsg());
			throw nullStockException;
		}

		BigDecimal fixedDividend = stock.getFixedDividend();
		BigDecimal parValue = stock.getParValue();
		BigDecimal tickerPrice = stock.getTickerPrice();

		// return (fixedDividend x parValue) / tickerPrice
		return (fixedDividend.multiply(parValue)).divide(tickerPrice, 2,
				BigDecimal.ROUND_HALF_UP);
	}

}
