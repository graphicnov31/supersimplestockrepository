package com.jpmorgan.supersimplestocks.businessrule;

import com.jpmorgan.supersimplestocks.business.model.StockType;

/**
 * A factory for creating CalculateDividendYieldBR objects.
 */
public interface CalculateDividendYieldBRFactory {

	/**
	 * Returns the CalculateDividendYieldBR according to stockType.
	 * 
	 * @param stockType
	 *            the stock type
	 * @return
	 */
	CalculateDividendYieldBR calculateDividendYieldBRFactory(StockType stockType);

}
