package com.jpmorgan.supersimplestocks.businessrule;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.service.action.CalculateDividendYieldActionImpl;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;

/**
 * The Class CalculateDividendYieldForCommonBRImpl.
 */
public class CalculateDividendYieldForCommonBRImpl implements
		CalculateDividendYieldBR {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculateDividendYieldActionImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.businessrule.CalculateDIvidendYieldBR#calculateDividendYield
	 * (com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public BigDecimal calculateDividendYield(Stock stock)
			throws ExecuteException {
		// check if stock is null
		if (stock == null) {
			NullStockException nullStockException = new NullStockException();
			LOGGER.error(nullStockException.getMsg());
			throw nullStockException;
		}

		BigDecimal lastDividend = stock.getLastDividend();
		BigDecimal tickerPrice = stock.getTickerPrice();

		// return lastDividend / tickerPrice
		return lastDividend.divide(tickerPrice, 2, BigDecimal.ROUND_HALF_UP);
	}

}