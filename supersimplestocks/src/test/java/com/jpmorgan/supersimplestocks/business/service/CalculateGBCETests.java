package com.jpmorgan.supersimplestocks.business.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.StockType;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/application-test-context.xml")
public class CalculateGBCETests {

	@Autowired
	private StockBusinessService stockBusinessService;

	@Test
	public void calculateGBCETest() throws ExecuteException {
		// create first stock COMMON
		String stockSimbol = "TEA";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.ZERO;
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(2);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		// first trade info
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, -1);
		Date timestamp = cal.getTime();
		int sharesQuantity = 4;
		TradeIndicatorType indicatorType = TradeIndicatorType.BUY;
		BigDecimal price = BigDecimal.valueOf(11.60);

		// record first trade for stock
		stockBusinessService.recordTrade(stock, timestamp, sharesQuantity,
				indicatorType, price);

		// second trade info
		cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -140);
		Date timestamp2 = cal.getTime();
		int sharesQuantity2 = 8;
		TradeIndicatorType indicatorType2 = TradeIndicatorType.SELL;
		BigDecimal price2 = BigDecimal.valueOf(15.60);

		// record second trade for stock
		stockBusinessService.recordTrade(stock, timestamp2, sharesQuantity2,
				indicatorType2, price2);

		// third trade info
		cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -125);
		Date timestamp3 = cal.getTime();
		int sharesQuantity3 = 6;
		TradeIndicatorType indicatorType3 = TradeIndicatorType.SELL;
		BigDecimal price3 = BigDecimal.valueOf(19.30);

		// record third trade for stock
		stockBusinessService.recordTrade(stock, timestamp3, sharesQuantity3,
				indicatorType3, price3);

		// create second stock PREFERRED
		String stockSimbol2 = "GIN";
		StockType stockType2 = StockType.PREFERRED;
		BigDecimal lastDividend2 = BigDecimal.valueOf(8);
		BigDecimal fixedDividend2 = BigDecimal.valueOf(2);
		BigDecimal parValue2 = BigDecimal.valueOf(100);
		BigDecimal tickerPrice2 = BigDecimal.valueOf(8.90);
		Stock secondStock = new Stock(stockSimbol2, stockType2, lastDividend2,
				fixedDividend2, parValue2, tickerPrice2);

		// trade info for second stock
		cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -130);
		Date timestamp21 = cal.getTime();
		int sharesQuantity21 = 9;
		TradeIndicatorType indicatorType21 = TradeIndicatorType.SELL;
		BigDecimal price21 = BigDecimal.valueOf(12);

		// record trade for second stock
		stockBusinessService.recordTrade(secondStock, timestamp21,
				sharesQuantity21, indicatorType21, price21);

		// second trade info for second stock
		cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -150);
		Date timestamp22 = cal.getTime();
		int sharesQuantity22 = 12;
		TradeIndicatorType indicatorType22 = TradeIndicatorType.BUY;
		BigDecimal price22 = BigDecimal.valueOf(22.90);

		// record second trade for second stock
		stockBusinessService.recordTrade(secondStock, timestamp22,
				sharesQuantity22, indicatorType22, price22);

		// calculate GBCE
		Calendar startTime = Calendar.getInstance();
		startTime.add(Calendar.HOUR_OF_DAY, -3);
		Calendar endTime = Calendar.getInstance();
		endTime.add(Calendar.HOUR_OF_DAY, -2);
		BigDecimal gBCE = stockBusinessService.calculateGBCE(
				startTime.getTime(), endTime.getTime());

		// test
		assertEquals(BigDecimal.valueOf(17.70).setScale(2), gBCE);
	}

}
