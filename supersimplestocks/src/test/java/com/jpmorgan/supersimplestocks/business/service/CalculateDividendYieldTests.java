package com.jpmorgan.supersimplestocks.business.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.StockType;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/application-test-context.xml")
public class CalculateDividendYieldTests {

	@Autowired
	private StockBusinessService stockBusinessService;

	@Test(expected = NullStockException.class)
	public void calculateDividendYieldFaildeTest() throws ExecuteException {

		// stock is null
		Stock stock = null;

		// calculate dividend yield
		stockBusinessService.calculateDividendYield(stock);
	}

	@Test
	public void calculateDividendYieldForCommonProductTest1() {
		// create stock COMMON
		String stockSimbol = "TEA";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.ZERO;
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(2);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		try {
			// calculate dividend yield
			BigDecimal calculateDividendYield = stockBusinessService
					.calculateDividendYield(stock);

			// test
			assertEquals(BigDecimal.ZERO.setScale(2), calculateDividendYield);
		} catch (ExecuteException e) {
			fail();
		}

	}

	@Test
	public void calculateDividendYieldForCommonProductTest2() {
		// create stock COMMON
		String stockSimbol = "POP";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.valueOf(8);
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(2);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		try {
			// calculate dividend yield
			BigDecimal calculateDividendYield = stockBusinessService
					.calculateDividendYield(stock);

			// test
			assertEquals(BigDecimal.valueOf(4).setScale(2),
					calculateDividendYield);
		} catch (ExecuteException e) {
			fail();
		}

	}

	@Test
	public void calculateDividendYieldForCommonProductTest3() {
		// create stock COMMON
		String stockSimbol = "ALE";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.valueOf(23);
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(60);
		BigDecimal tickerPrice = BigDecimal.valueOf(2.95);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		try {
			// calculate dividend yield
			BigDecimal calculateDividendYield = stockBusinessService
					.calculateDividendYield(stock);

			// test
			assertEquals(BigDecimal.valueOf(7.80).setScale(2),
					calculateDividendYield);
		} catch (ExecuteException e) {
			fail();
		}
	}

	@Test
	public void calculateDividendYieldForCommonProductTest4() {
		// create stock COMMON
		String stockSimbol = "JOE";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.valueOf(13);
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(250);
		BigDecimal tickerPrice = BigDecimal.valueOf(10.85);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		try {
			// calculate dividend yield
			BigDecimal calculateDividendYield = stockBusinessService
					.calculateDividendYield(stock);

			// test
			assertEquals(BigDecimal.valueOf(1.20).setScale(2),
					calculateDividendYield);
		} catch (ExecuteException e) {
			fail();
		}
	}

	@Test
	public void calculateDividendYieldForPreferredProductTest1() {
		// create stock PREFERRED
		String stockSimbol = "GIN";
		StockType stockType = StockType.PREFERRED;
		BigDecimal lastDividend = BigDecimal.valueOf(8);
		BigDecimal fixedDividend = BigDecimal.valueOf(2);
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(6.70);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		try {
			// calculate dividend yield
			BigDecimal calculateDividendYield = stockBusinessService
					.calculateDividendYield(stock);

			// test
			assertEquals(BigDecimal.valueOf(29.85).setScale(2),
					calculateDividendYield);
		} catch (ExecuteException e) {
			fail();
		}
	}

}
