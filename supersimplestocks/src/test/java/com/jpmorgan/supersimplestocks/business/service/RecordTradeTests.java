package com.jpmorgan.supersimplestocks.business.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.StockType;
import com.jpmorgan.supersimplestocks.business.model.Trade;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.entity.service.StockEntityService;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/application-test-context.xml")
public class RecordTradeTests {

	@Autowired
	private StockBusinessService stockBusinessService;

	@Autowired
	private StockEntityService stockEntityService;

	@Test(expected = NullStockException.class)
	public void recordTradeFailedTest() throws ExecuteException {
		// create stock
		Stock stock = null;

		// trade info
		Date timestamp = new Date();
		int sharesQuantity = 4;
		TradeIndicatorType indicatorType = TradeIndicatorType.BUY;
		BigDecimal price = BigDecimal.valueOf(11.60);

		// record trade
		stockBusinessService.recordTrade(stock, timestamp, sharesQuantity,
				indicatorType, price);

	}

	@Test
	public void recordTradeBasicTest() throws ExecuteException {
		// create stock COMMON
		String stockSimbol = "TEA";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.ZERO;
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(2);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		// trade info
		Date timestamp = new Date();
		int sharesQuantity = 4;
		TradeIndicatorType indicatorType = TradeIndicatorType.BUY;
		BigDecimal price = BigDecimal.valueOf(11.60);

		// record trade
		stockBusinessService.recordTrade(stock, timestamp, sharesQuantity,
				indicatorType, price);

		// test
		Collection<Trade> tradeCollection = stockEntityService
				.getTradesByStock(stock);
		assertEquals(1, tradeCollection.size());
		Trade trade = tradeCollection.iterator().next();
		assertEquals(timestamp, trade.getTimestamp());
		assertEquals(sharesQuantity, trade.getSharesQuantity());
		assertEquals(indicatorType, trade.getIndicatorType());
		assertEquals(price, trade.getPrice());

	}

	@Test
	public void recordTradeWith2StocksTest() throws ParseException,
			ExecuteException {
		// create first stock COMMON
		String stockSimbol = "TEA";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.ZERO;
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(2);
		Stock firstStock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		// trade info for first stock
		Date timestamp = new Date();
		int sharesQuantity = 4;
		TradeIndicatorType indicatorType = TradeIndicatorType.BUY;
		BigDecimal price = BigDecimal.valueOf(11.60);

		// record trade for first stock
		stockBusinessService.recordTrade(firstStock, timestamp, sharesQuantity,
				indicatorType, price);

		// create second stock PREFERRED
		String stockSimbol2 = "GIN";
		StockType stockType2 = StockType.PREFERRED;
		BigDecimal lastDividend2 = BigDecimal.valueOf(8);
		BigDecimal fixedDividend2 = BigDecimal.valueOf(2);
		BigDecimal parValue2 = BigDecimal.valueOf(100);
		BigDecimal tickerPrice2 = BigDecimal.valueOf(8.90);
		Stock secondStock = new Stock(stockSimbol2, stockType2, lastDividend2,
				fixedDividend2, parValue2, tickerPrice2);

		// trade info for second stock
		Date timestamp2 = new Date();
		int sharesQuantity2 = 8;
		TradeIndicatorType indicatorType2 = TradeIndicatorType.SELL;
		BigDecimal price2 = BigDecimal.valueOf(15.60);

		// record trade for second stock
		stockBusinessService.recordTrade(secondStock, timestamp2,
				sharesQuantity2, indicatorType2, price2);

		// test for first stock
		Collection<Trade> tradeCollection = stockEntityService
				.getTradesByStock(firstStock);
		assertEquals(1, tradeCollection.size());
		Trade trade = tradeCollection.iterator().next();
		assertEquals(timestamp, trade.getTimestamp());
		assertEquals(sharesQuantity, trade.getSharesQuantity());
		assertEquals(indicatorType, trade.getIndicatorType());
		assertEquals(price, trade.getPrice());

		// test for second stock
		Collection<Trade> tradeCollection2 = stockEntityService
				.getTradesByStock(secondStock);
		assertEquals(1, tradeCollection2.size());
		Trade trade2 = tradeCollection2.iterator().next();
		assertEquals(timestamp2, trade2.getTimestamp());
		assertEquals(sharesQuantity2, trade2.getSharesQuantity());
		assertEquals(indicatorType2, trade2.getIndicatorType());
		assertEquals(price2, trade2.getPrice());

	}

	@Test
	public void recordTradeWith2TradesFor1StockTest() throws ParseException,
			ExecuteException {
		// create first stock COMMON
		String stockSimbol = "TEA";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.ZERO;
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(2);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		// first trade info
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, -1);
		Date timestamp = cal.getTime();
		int sharesQuantity = 4;
		TradeIndicatorType indicatorType = TradeIndicatorType.BUY;
		BigDecimal price = BigDecimal.valueOf(11.60);

		// record first trade for stock
		stockBusinessService.recordTrade(stock, timestamp, sharesQuantity,
				indicatorType, price);

		// second trade info
		Date timestamp2 = new Date();
		int sharesQuantity2 = 8;
		TradeIndicatorType indicatorType2 = TradeIndicatorType.SELL;
		BigDecimal price2 = BigDecimal.valueOf(15.60);

		// record second trade for stock
		stockBusinessService.recordTrade(stock, timestamp2, sharesQuantity2,
				indicatorType2, price2);

		// test for first trade
		Collection<Trade> tradeCollection = stockEntityService
				.getTradesByStock(stock);
		assertEquals(2, tradeCollection.size());
		ArrayList<Trade> tradeList = new ArrayList<Trade>(tradeCollection);
		Collections.sort(tradeList, new Comparator<Trade>() {

			@Override
			public int compare(Trade o1, Trade o2) {
				return o1.getTimestamp().compareTo(o2.getTimestamp());
			}
		});
		Iterator<Trade> iterator = tradeList.iterator();
		Trade trade = iterator.next();
		assertEquals(timestamp, trade.getTimestamp());
		assertEquals(sharesQuantity, trade.getSharesQuantity());
		assertEquals(indicatorType, trade.getIndicatorType());
		assertEquals(price, trade.getPrice());

		// test for second trade
		Trade trade2 = iterator.next();
		assertEquals(timestamp2, trade2.getTimestamp());
		assertEquals(sharesQuantity2, trade2.getSharesQuantity());
		assertEquals(indicatorType2, trade2.getIndicatorType());
		assertEquals(price2, trade2.getPrice());

	}

}
