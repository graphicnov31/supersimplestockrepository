package com.jpmorgan.supersimplestocks.business.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.StockType;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/application-test-context.xml")
public class CalculateStockPriceTests {

	@Autowired
	private StockBusinessService stockBusinessService;

	@Test
	public void calculateStockPriceTest() throws ExecuteException {
		// create first stock COMMON
		String stockSimbol = "TEA";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.ZERO;
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(2);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		// first trade info
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, -1);
		Date timestamp = cal.getTime();
		int sharesQuantity = 4;
		TradeIndicatorType indicatorType = TradeIndicatorType.BUY;
		BigDecimal price = BigDecimal.valueOf(11.60);

		// record first trade for stock
		stockBusinessService.recordTrade(stock, timestamp, sharesQuantity,
				indicatorType, price);

		// second trade info
		cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -1);
		Date timestamp2 = cal.getTime();
		int sharesQuantity2 = 8;
		TradeIndicatorType indicatorType2 = TradeIndicatorType.SELL;
		BigDecimal price2 = BigDecimal.valueOf(15.60);

		// record second trade for stock
		stockBusinessService.recordTrade(stock, timestamp2, sharesQuantity2,
				indicatorType2, price2);

		// third trade info
		cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -10);
		Date timestamp3 = cal.getTime();
		int sharesQuantity3 = 6;
		TradeIndicatorType indicatorType3 = TradeIndicatorType.SELL;
		BigDecimal price3 = BigDecimal.valueOf(19.30);

		// record third trade for stock
		stockBusinessService.recordTrade(stock, timestamp3, sharesQuantity3,
				indicatorType3, price3);

		// calculate stock price
		// the first trade is out of range
		cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -30);
		BigDecimal stockPrice = stockBusinessService.calculateStockPrice(stock,
				cal.getTime(), new Date());

		// test
		assertEquals(BigDecimal.valueOf(17.19).setScale(2), stockPrice);

	}

}
