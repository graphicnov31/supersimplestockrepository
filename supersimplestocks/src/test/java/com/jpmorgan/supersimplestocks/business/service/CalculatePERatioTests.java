package com.jpmorgan.supersimplestocks.business.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.StockType;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/application-test-context.xml")
public class CalculatePERatioTests {

	@Autowired
	private StockBusinessService stockBusinessService;

	@Test
	public void calculatePERationForCommonProductTest() {
		// create stock COMMON
		String stockSimbol = "POP";
		StockType stockType = StockType.COMMON;
		BigDecimal lastDividend = BigDecimal.valueOf(8);
		BigDecimal fixedDividend = null;
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(4.50);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		try {
			// calculate P/E Ration
			BigDecimal pERatio = stockBusinessService.calculatePERatio(stock);

			// test
			assertEquals(BigDecimal.valueOf(2.53).setScale(2), pERatio);
		} catch (ExecuteException e) {
			fail();
		}
	}

	@Test
	public void calculatePERationForPreferredProductTest() {
		// create stock PREFERRED
		String stockSimbol = "GIN";
		StockType stockType = StockType.PREFERRED;
		BigDecimal lastDividend = BigDecimal.valueOf(8);
		BigDecimal fixedDividend = BigDecimal.valueOf(2);
		BigDecimal parValue = BigDecimal.valueOf(100);
		BigDecimal tickerPrice = BigDecimal.valueOf(15.70);
		Stock stock = new Stock(stockSimbol, stockType, lastDividend,
				fixedDividend, parValue, tickerPrice);

		try {
			// calculate P/E Ration
			BigDecimal pERatio = stockBusinessService.calculatePERatio(stock);

			// test
			assertEquals(BigDecimal.valueOf(1.23).setScale(2), pERatio);
		} catch (ExecuteException e) {
			fail();
		}
	}

}
